"""
Author: Apu Kumar Chakroborti
"""
from django.urls import path
from . import views
from payment.views import create

urlpatterns = [
    path('paymentbkash/', create, name='create'),
]
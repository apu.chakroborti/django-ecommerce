#Create ecommerce website fro manascode tutorial

#create project
cd /path/to/the/project/
django-admin startproject ecommerce
python3.6 manage.py startapp products


#postgres database installation
sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'

sudo apt-get update
sudo apt-get install postgresql postgresql-contrib

#Connect to PostgreSQL
sudo su - postgres
psql


\conninfo

#for exit
\q

#database creation

sudo su - postgres
psql

#change password
\password postgres
password:tigerit

#use the command below for database creation or use pg admin
CREATE DATABSE ecommerce-manascode



#install pillow
pip3 install Pillow

#data migration
sudo apt install python3-psycopg2

python3 manage.py makemigrations
python3 manage.py migrate


#run the server

python3 manage.py runserver

#install mysql client database
pip3 install -U mysqlclient


virtualenv env
source env/bin/activate

pip3 install -r requirements.txt

#create super user for first admin login
------------------------------------------
python3 manage.py createsuperuser
username:apu
Email:apukumar220@gmail.com
password:tigerit123

#creating cart app
---------------------------------
python3.6 manage.py startapp cart
add it to the settings.py INSTALLED_APP:

python3.6 manage.py makemigrations cart
python3.6 manage.py migrate cart

#all-auth addition
-----------------------------------
pip3 install django-allauth

#creating checkout app
---------------------------------
python3.6 manage.py startapp checkout
add it to the settings.py INSTALLED_APP:

python3.6 manage.py makemigrations checkout
python3.6 manage.py migrate checkout


#bkash payment addition
---------------------------
pip3 install django-bkash-integration

python3.6 manage.py startapp payment

add it to the settings.py INSTALLED_APP:

python3.6 manage.py makemigrations bkash
python3.6 manage.py migrate bkash

